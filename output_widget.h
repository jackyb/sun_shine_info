#ifndef OUTPUT_WIDGET_H
#define OUTPUT_WIDGET_H

#include "sun_shine_info.h"

#include <QWidget>

class OutputWidget : public QWidget {
public:
	OutputWidget(QWidget *parent = 0);

	void setData(const SunShineInfo &info, const QString key);

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
	SunShineInfo info;
	QString key;
};

#endif // OUTPUT_WIDGET_H
