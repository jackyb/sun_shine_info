#include "main_window.h"
#include "ui_main_window.h"
#include "output_widget.h"

#include <QApplication>
#include <QKeyEvent>
#include <QFile>
#include <QPrinter>
#include <QPrinterInfo>
#include <QPainter>
#include <QFont>
#include <QMessageBox>
#include <QAbstractButton>
#include <QTimer>
#include <QDebug>

// If use printer, graphics will be printed. If not, it will be displayed on the screen;
static const auto USE_PRINTER = false;

static const auto BUF_LEN = 4096;
static const auto DATE_SEPARATOR = "\u2013"; // en dash

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	currentInput(0),
	backToMainScreenTimer(0),
	canGoBack(false),
	isShowingOutputOnScreen(false)
{
	// Setup UI
	setupUi();

	// Load sun shine data
	load();

	// Set fullscreen
	setFixedSize(640, 480);
	showFullScreen();
}

MainWindow::~MainWindow() {
	delete ui;
}

bool MainWindow::event(QEvent *event) {
	if (event->type() == QEvent::KeyPress) {
		auto ke = static_cast<QKeyEvent *>(event);
		if (ke->key() == Qt::Key_Escape) {
			onEscapePressed();
		}
	} else if (event->type() == QEvent::KeyRelease) {
		auto ke = static_cast<QKeyEvent *>(event);
		if (ke->key() == Qt::Key_Backspace || ke->key() == Qt::Key_Clear) {
			onBackspaceReleased();
		} else {
			onKeyReleased(ke->key());
		}
	}

	return QMainWindow::event(event);
}

void MainWindow::setupUi() {
	// Setup UI
	ui->setupUi(this);

	// Set input validators
	ui->yearEdit->setValidator(new QIntValidator(0, 2099, this));
	ui->monthEdit->setValidator(new QIntValidator(1, 12, this));
	ui->dayEdit->setValidator(new QIntValidator(1, 31, this));
	ui->dayEdit->setFocus();

	// Set message boxes
	QFont font;
	font.setBold(true);
	font.setPointSize(24);

	noRecordMsg = new QMessageBox(QMessageBox::Information, "", "<font size='24'>No records on this date.</font>", QMessageBox::Ok);
	auto buttons = noRecordMsg->buttons();
	foreach (auto btn, buttons) {
		btn->setFont(font);
	}

	noPrinterMsg = new QMessageBox(QMessageBox::Information, "", "<font size='24'>Could not find the printer.</font>", QMessageBox::Ok);
	buttons = noRecordMsg->buttons();
	foreach (auto btn, buttons) {
		btn->setFont(font);
	}

	// Set global focus listener
	auto app = static_cast<QApplication *>(QCoreApplication::instance());
	connect(app, SIGNAL(focusChanged(QWidget *, QWidget *)), this, SLOT(onFocusChanged(QWidget *, QWidget *)));
}

// Load sun shine data from a .csv file
void MainWindow::load() {
	char buf[BUF_LEN];
	qint64 n = 0;

	QFile file(":/data.csv");
	if (!file.open(QIODevice::ReadOnly)) {
		qDebug() << "Failed to open file";
		return;
	}

	auto first = true;
	while ((n = file.readLine(buf, BUF_LEN)) != -1) {
		if (first) {
			first = false;
			continue;
		}
		QString line(buf);
		auto tokens = line.split(",");
		auto info = SunShineInfo::parseStringList(tokens);
		auto key = QString::number(info.day) + DATE_SEPARATOR +
				QString::number(info.month) + DATE_SEPARATOR +
				QString::number(info.year);
		sunShineInfos[key] = info;
	}
}

void MainWindow::onFocusChanged(QWidget *prev, QWidget *curr) {
	currentInput = curr;
}

void MainWindow::onEscapePressed() {
	resetInput();
}

void MainWindow::onBackspaceReleased() {
	if (currentInput == ui->monthEdit) {
		if (ui->monthEdit->text().length() == 0) {
			if (canGoBack) {
				ui->dayEdit->setFocus();
				canGoBack = false;
				return;
			}
			canGoBack = true;
		}
	} else if (currentInput == ui->yearEdit) {
		if (ui->yearEdit->text().length() == 0) {
			if (canGoBack) {
				ui->monthEdit->setFocus();
				canGoBack = false;
				return;
			}
			canGoBack = true;
		}
	}
}

void MainWindow::onKeyReleased(int key) {
	if (!USE_PRINTER && isShowingOutputOnScreen) {
		backToMainScreen();
	}

	if (currentInput == ui->dayEdit) {
		if (ui->dayEdit->text().length() == 2) {
			ui->monthEdit->setFocus();
			canGoBack = true;
		}
	} else if (currentInput == ui->monthEdit) {
		if (ui->monthEdit->text().length() == 2) {
			ui->yearEdit->setFocus();
			canGoBack = true;
		} else {
			canGoBack = false;
		}
	} else if (currentInput == ui->yearEdit) {
		if (ui->yearEdit->text().length() > 0) {
			if (key == Qt::Key_Enter || key == Qt::Key_Return) {
				if (USE_PRINTER) {
					showOutputOnPrint();
				} else {
					showOutputOnScreen();
				}
				resetInput();
			}
		} else {
			canGoBack = false;
		}
	}
}

// Show sun shine info visualization on print
void MainWindow::showOutputOnPrint() {
	// Get date from input
	auto year = QString::number(ui->yearEdit->text().toInt());
	auto month = QString::number(ui->monthEdit->text().toInt());
	auto day = QString::number(ui->dayEdit->text().toInt());

	// Get SunShineInfo from the hash map
	auto key = day + DATE_SEPARATOR + month + DATE_SEPARATOR + year;
	if (!sunShineInfos.contains(key)) {
		noRecordMsg->show();
		return;
	}

	// Find printer
	auto printerInfos = QPrinterInfo::availablePrinters();
	auto printerInfo = QPrinterInfo();
	auto foundPrinter = false;
	foreach (auto info, printerInfos) {
		if (info.printerName().contains("EPSON")) {
			printerInfo = info;
			foundPrinter = true;
		}
	}

	// Use any printer if no printer with matching name is found
	if (!foundPrinter) {
		if (printerInfos.size() > 0) {
			printerInfo = printerInfos[0];
			foundPrinter = true;
		} else {
            noPrinterMsg->show();
			return;
		}
	}

	auto sunShineInfo = sunShineInfos[key];

	// Set printer
	QPrinter printer(printerInfo);

	// Set painter
	QPainter painter(&printer);
	auto rect = painter.viewport();
	painter.setViewport(rect);
	painter.setWindow(rect);

	// Set font for main text
	QFont font;
	font.setBold(true);
	font.setPointSize(10);
	painter.setFont(font);

	// Draw template image
	QImage image(":/template.png");
	image = image.scaledToWidth(rect.width());
	painter.drawImage(image.rect(), image);

	// Draw outline
	auto outline = image.rect();
	outline.adjust(0, 0, -2, -2); // adjust a little because Windows doesn't print the outline on the right and bottom
	painter.drawRect(outline);

	// Draw the main text
	painter.translate(image.rect().bottomLeft() * 0.25);
	QRect textRect(image.rect().width() * 0.25, 0, image.rect().width() * 0.5, image.rect().height() * 0.3);
	QTextOption option(Qt::AlignCenter);
	painter.drawText(textRect, "ON THIS DATE\n" + key + "\nthe Sun shone for\n" + QString::number(sunShineInfo.hours[12]) + " hours", option);

	// Set font for graph text
	font.setPointSize(6);
	painter.setFont(font);

	// Draw the graph
	painter.translate(image.rect().bottomLeft() * 0.25);
	for (auto i = 0; i <= 11; i++) {
		// Draw bar for each hour
		auto hp = sunShineInfo.hours[i];
		auto x = (int) ((i / 12.0f) * rect.width());
		auto y = 100;
		auto width = (int) (1 / 12.0f * rect.width());
		auto height = (int) (-hp * y);
		painter.fillRect(x, y, width, height, QColor(0, 0, 0));

		// Draw hour marker
		painter.drawLine(x, y - 5, x, y + 5);

		// Draw label for certain hours
		if (i == 0) {
			painter.drawText(x, y + 20, "7am");
		} else if (i == 6) {
			painter.drawText(x, y + 20, "12nn");
		} else if (i == 11) {
			painter.drawText(x, y + 20, "6pm");
		}
	}

	// Finally send result for printing
	painter.end();
}

// Show sun shine info visualization on screen
void MainWindow::showOutputOnScreen() {
	// Get date from input
	auto year = QString::number(ui->yearEdit->text().toInt());
	auto month = QString::number(ui->monthEdit->text().toInt());
	auto day = QString::number(ui->dayEdit->text().toInt());

	// Get SunShineInfo from the hash map
	auto key = day + DATE_SEPARATOR + month + DATE_SEPARATOR + year;
	if (!sunShineInfos.contains(key)) {
		noRecordMsg->show();
		return;
	}

	ui->centralwidget->hide();

	auto outputWidget = new OutputWidget;
	outputWidget->setFixedSize(640, 480);
	setCentralWidget(outputWidget);
	outputWidget->setData(sunShineInfos[key], key);
	outputWidget->show();

	isShowingOutputOnScreen = true;

	if (backToMainScreenTimer) {
		delete backToMainScreenTimer;
	}
	backToMainScreenTimer = new QTimer(this);
	backToMainScreenTimer->setSingleShot(true);
	backToMainScreenTimer->setInterval(30 * 1000);
	connect(backToMainScreenTimer, SIGNAL(timeout()), this, SLOT(backToMainScreen()));
	backToMainScreenTimer->start();
}

void MainWindow::resetInput() {
	ui->dayEdit->setText("");
	ui->monthEdit->setText("");
	ui->yearEdit->setText("");
	ui->dayEdit->setFocus();
}

void MainWindow::backToMainScreen() {
	if (isShowingOutputOnScreen) {
		isShowingOutputOnScreen = false;
		setupUi();
	}
}
