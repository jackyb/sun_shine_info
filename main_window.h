#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "sun_shine_info.h"

#include <QMainWindow>
#include <QHash>

namespace Ui {
	class MainWindow;
};

class QWidget;
class QMessageBox;
class QTimer;

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
	bool event(QEvent *event);

private:
	QHash<QString, SunShineInfo> sunShineInfos;

	Ui::MainWindow *ui;
	QWidget *currentInput;
	QMessageBox *noRecordMsg;
	QMessageBox *noPrinterMsg;
	QTimer *backToMainScreenTimer;

	// Determine if user should go to previous input after pressing backspace when deleting input
	bool canGoBack;

	// Is the data visualization currently showing on the screen?
	bool isShowingOutputOnScreen;

	void setupUi();
	void load();

private slots:
	void onFocusChanged(QWidget *, QWidget *);
	void onEscapePressed();
	void onBackspaceReleased();
	void onKeyReleased(int);

	void showOutputOnPrint();
	void showOutputOnScreen();
	void resetInput();
	void backToMainScreen();
};

#endif
