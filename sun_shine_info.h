#ifndef SUN_SHINE_INFO_H
#define SUN_SHINE_INFO_H

#include <QStringList>

struct SunShineInfo {
	int year;
	int month;
	int day;
	float hours[13];

	static SunShineInfo parseStringList(QStringList tokens) {
		SunShineInfo info;

		info.year = tokens[1].toInt();
		info.month = tokens[2].toInt();
		info.day = tokens[3].toInt();
		for (int i = 0 ; i < 13; i++) {
			info.hours[i] = tokens[4 + i].toFloat();
		}

		return info;
	}
};

#endif
