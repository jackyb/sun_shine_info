Sun Shine Info
==============
A software that allows user to input his birth date and results in data visualization printed by thermal printer that displays how long the Sun shone during his birth day.

Instructions
------------
Install [Qt 5](www.qt.io). Launch Qt Creator and open the project then proceed to run it through Build > Run (or press Ctrl + R). The thermal printer is recognized as normal printer so it can be controlled through the standard printing interface.
