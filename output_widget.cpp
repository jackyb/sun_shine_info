#include "output_widget.h"

#include <QPainter>
#include <QLayout>
#include <QtMath>

#define SMALLER_TEXT
//#define LARGER_TEXT

OutputWidget::OutputWidget(QWidget *parent) :
	QWidget(parent)
{
}

void OutputWidget::setData(const SunShineInfo &info, const QString key) {
	this->info = info;
	this->key = key;
}

void OutputWidget::paintEvent(QPaintEvent *event) {
	if (key.isNull() || key.isEmpty()) {
		return;
	}

	auto sunShineInfo = this->info;

	// Set painter
	QPainter painter(this);
	auto rect = painter.viewport();
	painter.setPen(QPen(QColor(255, 255, 255)));
	painter.setViewport(rect);
	painter.setWindow(rect);
	painter.fillRect(0, 0, rect.width(), rect.height(), QColor(0, 0, 0));

	// Draw outline
	auto outline = rect;
	outline.adjust(0, 0, -2, -2); // adjust a little because Windows doesn't print the outline on the right and bottom
	painter.drawRect(outline);

	// Set font for main text
	QFont font;
	font.setBold(true);
#ifdef SMALLER_TEXT
	font.setPointSize(20);
#elif defined(LARGER_TEXT)
	font.setPointSize(28);
#else
	font.setPointSize((24));
#endif
	painter.setFont(font);

	// Draw the main text
#ifdef SMALLER_TEXT
	QRect textRect(0, rect.height() * -0.05, rect.width(), rect.height() * 0.3);
#elif defined(LARGER_TEXT)
	QRect textRect(0, 0, rect.width(), rect.height() * 0.3);
#else
	QRect textRect(0, rect.height() * -0.025, rect.width(), rect.height() * 0.3);
#endif
	QTextOption option(Qt::AlignCenter);
	painter.drawText(textRect, "On " + key + ",\nthe Sun shone for " + QString::number(sunShineInfo.hours[12]) + " hours.", option);

	// Set font for graph text
#ifdef SMALLER_TEXT
	font.setPointSize(12);
#elif defined(LARGER_TEXT)
	font.setPointSize(20);
#else
	font.setPointSize(16);
#endif
	painter.setFont(font);

	// Draw the graph
	painter.translate(rect.bottomLeft() * 0.275);
	for (auto i = 0; i <= 11; i++) {
		// Draw bar for each hour
		auto hp = sunShineInfo.hours[i];
		auto x = 50 + 45 * i;
		auto y = 300;
		auto width = 44;
		auto height = (-hp * 270);
		painter.fillRect(x, y, width, -270, QBrush(QColor(255,255,255), Qt::BDiagPattern));
		painter.fillRect(x, y, width, height, QColor(255,255,255));

		// Draw hour marker
		painter.drawLine(x, y - 5, x, y + 5);

		// Draw label for certain hours
		if (i == 0) {
#ifdef SMALLER_TEXT
			painter.drawText(x - 15, y + 27, "7am");
#elif defined(LARGER_TEXT)
			painter.drawText(x - 30, y + 27, "7am");
#else
			painter.drawText(x - 25, y + 27, "7am");
#endif
		} else if (i == 6) {
#ifdef SMALLER_TEXT
			painter.drawText(x - 30, y + 27, "12nn");
#elif defined(LARGER_TEXT)
			painter.drawText(x - 45, y + 27, "12nn");
#else
			painter.drawText(x - 30, y + 27, "12nn");
#endif
		} else if (i == 11) {
#ifdef SMALLER_TEXT
			painter.drawText(x + 25, y + 27, "6pm");
#elif defined(LARGER_TEXT)
			painter.drawText(x - 0, y + 27, "6pm");
#else
			painter.drawText(x + 25, y + 27, "6pm");
#endif
		}
	}

	// Finally display result on screen
	painter.end();
}
